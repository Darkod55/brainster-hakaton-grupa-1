<?php

require_once __DIR__."/db/mypdo.php";

try
{
    $db = new MyPDO();
    $id = $_REQUEST['id'];
    $sql = "DELETE FROM AddCompany WHERE comp_id = :id";
    $sh = $db->prepare($sql);
    $sh->bindParam(":id",$id);
    $sh->execute();
    
    header('Location: dashboard.php?success=deleted');

} catch(PDOException $e)
{
    var_dump($e->getMessage());
}