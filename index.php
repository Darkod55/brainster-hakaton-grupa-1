<?php
session_start();
?>

<!DOCTYPE html>
<html>

<head>
    <title>
        Homepage
    </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Optimizacija za mobile-->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/project.css" rel="stylesheet">
    <link rel='icon' href='assets/images/favicon.ico' type='image/x-icon' />
    <script src="https://kit.fontawesome.com/28330b3406.js" crossorigin="anonymous"></script>
    <script>

    </script>

</head>

<body>
    <header>
        <nav class="navbar nav-margin navbar-fixed-top custom-nav">
            <div class="container-fluid top-header">
                
                <div class="navbar-header formobilep">
                <img class="logo navbar-left" src="assets/images/2HRtools@4x-8.png" />
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#fortoggle" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- CODE FOR MOBILE MENU - NOT FINISHED-->
                    <!-- <div class="collapse navbar-collapse menumobile" id="fortoggle">
                        <?php
                        //     include_once "forcategories.php";
                        //     foreach($results as $row) {
                        //         echo "<p data-catid='cat" . $row['cat_id'] . "' id='category" . $row['cat_id'] . "' class='cat-item'>" . $row['title_cat'] . "</p>";
                        //     }
                        //     echo "<p class='cat-item' id='addcompany'>ADD NEW COMPANY</p>";
                        // ?>
                        //     <div class="forLinkedin cat-item">
                        //     <?php 
                        //         if(isset($_SESSION['username'])) {
                        //             if($_SESSION['username'] == "admin") {
                        //                 echo "<p class='user-name'>Welcome " . $_SESSION['username'] . "</p>"; 
                        //                 echo " <p class='cat-item item-login'><a id='dashboard' href='dashboard.php'>Dashboard</a></p>";
                                        
                        //             } 
                        //             elseif($_SESSION['username'] !== "admin") {
                        //                 echo "<p class='user-name'>Welcome " . $_SESSION['username'] . "</p>"; 
                        //             } 
                        //         } else {
                        //             echo "<p class='cat-item item-login'><a id='forlogin'>Login</a></p>";
                        //     }
                            ?>
                            </div>
                    </div> -->
                    <form class="navbar-form nav-line forsearch">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="search-icon"><span class="glyphicon glyphicon-search"></span></span>
                                <input type="text" class="form-control for-search" aria-describedby="search">
                            </div>
                            <!-- <button type="submit" class="btn btn-default">Submit</button> -->
                        </div>
                    </form>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <form class="navbar-form navbar-right nomobile" action="app/api/api_subscribe.php" method="GET">
                        <div class="form-group custom-form-group navbar-right">
                            <div class="form-group flex custom-form-group">
                                <div class="input-group">
                                    <span class="input-group-addon custom-form-icon"><i class="far fa-envelope"></i></span>
                                    <input type="email" name="email" required class="form-control custom-form" placeholder="Get two new looks every week" aria-describedby="basic-addon1">
                                </div>
                                <button class="btn submitBtn" type="submit" value="Submit">Find out first!
                                </button>
                            </div>
                        </div>
                        <?php
                            require_once __DIR__.'/functions.php';
                            if(isset($_GET['error'])) {
                                echo errorMessage();
                            }
                            if(isset($_GET['success']) && $_GET['success'] == 'true') {
                                echo successMessage();
                            }
                        ?>
                    </form>
                </div>
            </div>
        </nav>
    </header>
    <div class="sidenav fordesktop">
        <?php
            include_once __DIR__."/forcategories.php";
            foreach($results as $row) {
                echo "<p data-catid='cat" . $row['cat_id'] . "' id='category" . $row['cat_id'] . "' class='cat-item'>" . $row['title_cat'] . "</p>";
            }
            echo "<p class='cat-item addthiscomp' id='addcompany'>ADD NEW COMPANY</p>";
        ?>
            <div class="forLinkedin cat-item">
            <?php 
                if(isset($_SESSION['username'])) {
                    if($_SESSION['username'] == "admin") {
                        echo "<p class='user-name'>Welcome " . $_SESSION['username'] . "</p>"; 
                        echo " <p class='cat-item item-login'><a id='dashboard' href='dashboard.php'>Dashboard</a></p>";
                        
                    } 
                    elseif($_SESSION['username'] !== "admin") {
                        echo "<p class='user-name'>Welcome " . $_SESSION['username'] . "</p>"; 
                    } 
                } else {
                    echo "<p class='item-login'><a id='forlogin'>Login</a></p>";
            }
            ?>
                <a href="https://www.linkedIn.com/" target="_blank"><i class="fab fa-linkedin-in fa-2x linkedIn"></i></a>
            </div>
    </div>
    <!-- <div class="container-fluid toolbar">
    <?php
        // include_once "toolbar.php";
        ?>
    </div> -->
    <div class="container-fluid" id="featured">
        
        <?php
        if(isset($_GET['success']) && $_GET['success'] == 'addedcomp') {
            echo successMessage();
        }
        include_once __DIR__."/forfeatured.php";
        ?>
    </div>
    <div class="container-fluid posts" id="forposts">
       <?php
        include_once __DIR__."/forposts.php";
       ?>
       
    </div>
    <div class="container-fluid addform">
    <?php
        include_once __DIR__."/foraddform.php";
       ?>
    </div>
    <div class="container-fluid loginbox" id="loginform">
    <?php
        include_once __DIR__."/forloginform.php";
        ?>
    </div>
</body>
<script src="assets/js/jquery-3.4.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="main.js"></script>
</html>
