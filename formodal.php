<?php

$username = "brainste_team1";
$password = "Team1Hackaton";

$db = new PDO('mysql:host=localhost;dbname=brainste_team1', $username, $password);

$cat = "SELECT * FROM (PostContent INNER JOIN Categories ON PostContent.category_id=Categories.id)";
$sh = $db->prepare($cat);
$sh->execute();
$results = $sh->fetchAll();


// echo "<pre>";
// print_r($results);


?>
<link href="assets/css/project.css" rel="stylesheet">
 <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
 <script src="https://kit.fontawesome.com/28330b3406.js" crossorigin="anonymous"></script>
 <?php 
    foreach($results as $row) { ?>
            <a href="" data-toggle="modal" data-target="#card<?= $row['category_id'] ?>">Test</a>
            <div class="modal fade modal-fullscreen"  id="card<?= $row['category_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="<?= $row['title'] ?>">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="sidenav sidemodal">
                            <p class="post-title"><?= $row['title']; ?></p>
                            <div class="for-cat">
                                <p class="post-cat post-title-modal"><?= $row['title_cat']; ?></p>
                                <hr style="background-color: <?= $row['color']; ?>"class="hr hr-modal pull-left">
                            </div>
                        </div>
                        <div class="container-fluid inmodal">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-3 formodal">
                                    <img class="cat-image" src="<?= $row['featured_image']; ?>" />
                                    <div class="for-content-post">
                                        <p class="post-title"><?= $row['title']; ?></p>
                                        <i class="fal fa-share-alt" id="forshare"></i><p id="testfield">
                                            <i class="fab fa-facebook"></i>
                                            <i class="fab fa-google-plus"></i>
                                            <i class="fab fa-instagram"></i>
                                            <i class="fab fa-reddit"></i>
                                            <i class="fab fa-twitter-square"></i>
                                            <i class="fab fa-linkedin-in"></i>
                                            </p>
                                        <p class="post-content"><?= $row['content']; ?></p>
                                        <form class="navbar-form form-in-modal">
                                            <div class="form-group custom-form-group">
                                                <div class="form-group flex custom-form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon custom-form-icon"><i class="far fa-envelope"></i></span>
                                                        <input type="email" required id="email" class="form-control custom-form"
                                                            placeholder="Get two new looks every week" aria-describedby="basic-addon1">
                                                    </div>
                                                        <button class="btn submitBtn" id="submit" type="submit" value="Submit">Stay Updated
                                                        </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>              
                                </div>
                            </div>
                        </div>
                        <div class="modal-body modal-size">
                            <button type="button" class="close close-per" data-dismiss="modal"
                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        </div>
                    </div>
                </div> 
            </div>
<?php }?>

    <script src="assets/js/jquery-3.4.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>