<div class="row">
        <div class="col-md-9 col-md-offset-3">
            <div id="login">
                 <?php
                    require_once __DIR__.'/functions.php';
                    if(isset($_GET['error']) && $_REQUEST['error'] == 'wronguser') {
                        echo errorMessage();
                    }
                    if(isset($_GET['success']) && $_REQUEST['success'] == 'login') {
                        echo successMessage();
                    }
                ?>
                <form class="addcompform" action="checkuser.php" method="POST">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" name="username" class="form-control margin-bottom" id="username" placeholder="Username">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control margin-bottom" id="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-default butaddcomp">Submit</button>
                </form>
            </div>
        </div>
    </div>