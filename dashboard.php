<?php
require_once __DIR__."/db/mypdo.php";
require_once __DIR__.'/functions.php';

$db = new MyPDO();
$us = "SELECT * FROM AddCompany";
$sh = $db->prepare($us);
$sh->execute();
$results = $sh->fetchAll();
echo "<button class='backhome btn btn-default butaddcomp'><a href='index.php'>Back to Homepage</a></button>";
if(isset($_REQUEST['error'])) {
    echo errorMessage();
}
if(isset($_REQUEST['success']) && $_REQUEST['success'] == 'updated') {
    echo successMessage();
}
if(isset($_REQUEST['success']) && $_REQUEST['success'] == 'deleted') {
    echo successMessage();
}
echo "<div class='container approvedcomp'>
        <div class='row'>
            <div class='col-md-12'>";
            echo "<h1>Approved Companies</h1>";
            echo "<table class='for-dashboard' border='1'>";
            echo "<tr>
                    <th>ID</th>
                    <th>Company Name</th>
                    <th>Website</th>
                    <th>About</th>
                    <th>Email</th>
                    <th>Approved</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <tr>";
            foreach($results as $row) {
                if($row['is_approved'] == 1) {
                echo "<tr>" .
                "<td>" . $row['comp_id'] . "</td>" . 
                "<td>" . $row['name'] . "</td>
                <td>" . $row['website'] . "</td>
                <td>" . $row['about'] . "</td>
                <td>" . $row['email'] . "</td>
                <td>" . $row['is_approved'] . "</td>
                <td><button class='btn btn-default butaddcomp'><a href='edit.php?id=".$row['comp_id'] ."'>Edit</a></button></td>
                <td><button class='btn btn-default butaddcomp'><a href='delete.php?id=".$row['comp_id'] ."'>Delete</a></button></td>" . 
                "</tr>"; 
                }
            }
            echo "</table>";
echo "</div>
    </div>
</div>";

echo "<hr>";
echo "<div class='container approvedcomp'>
        <div class='row'>
            <div class='col-md-12'>";
            echo "<h1>Pending Approval</h1>";
            echo "<table class='for-dashboard' border='1'>";
            echo "<tr>
                    <th>ID</th>
                    <th>Company Name</th>
                    <th>Website</th>
                    <th>About</th>
                    <th>Email</th>
                    <th>Approved</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <tr>";
            foreach($results as $row) {
                if($row['is_approved'] == 0) {
                echo "<tr>" .
                "<td>" . $row['comp_id'] . "</td>" . 
                "<td>" . $row['name'] . "</td>
                <td>" . $row['website'] . "</td>
                <td>" . $row['about'] . "</td>
                <td>" . $row['email'] . "</td>
                <td>" . $row['is_approved'] . "</td>
                <td><button class='btn btn-default butaddcomp'><a href='edit.php?id=".$row['comp_id'] ."'>Edit</a></button></td>
                <td><button class='btn btn-default butaddcomp'><a href='delete.php?id=".$row['comp_id'] ."'>Delete</a></button></td>" . 
                "</tr>"; 
                }
            }
            echo "</table>";
echo "</div>
    </div>
</div>";

?>

<html>
    <head>
        <link href="assets/css/project.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <script src="assets/js/jquery-3.4.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>