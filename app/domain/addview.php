<?php
require_once __DIR__."/../../db/mypdo.php";
class ForViews {
    public $id;
    private $db;
    private $views;
    private $date_creation;

    public function __construct($id)
    {
        $this->id = $id; 
    }

    public function updateViews($id) {
        $db = new MyPDO();
        $sub = "SELECT * FROM PostContent WHERE id  = $id";
        $sh = $db->prepare($sub);
        $sh->execute();
        $results = $sh->fetchAll();
        $currentviews = $results[0]['views'];
        $nextviews = $currentviews + 1;
        $v = "UPDATE PostContent SET `views` = :views WHERE id = :id";
        $args = ["views" => $nextviews, "id" => $id];
        $sh = $db->run($v, $args);
    }
}

?>